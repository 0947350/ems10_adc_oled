#include <msp430.h> 
#include <stdint.h>

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weergeven
 * temperatuur aan.
 *
 * int temp: De te weergeven temperatuur.
 * Max is 999 min is -99. Hierbuiten weer-
 * geeft het "Err-"
 *
 * Voorbeeld:
 * setTemp(100); //stel 10.0 graden in
 */
void setTemp(int temp);

/* Deze functie past de bovenste titel aan.
 * char tekst[]: de te weergeven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); //laat Hallo zien
 */
void setTitle(char tekst[]);

#pragma vector = ADC10_VECTOR;
__interrupt void ADC_to_temp(void)
{
    static int temperatuur = 0;
    if((ADC10CTL0 & REF2_5V) == 0)
    {
        temperatuur = ((ADC10MEM / 1023.0) * 1500);
    }
    else if((ADC10CTL0 & REF2_5V) != 0)
    {
        temperatuur = ((ADC10MEM / 1023.0) * 2500) * 100;
    }
    setTemp(temperatuur);
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void temp_timer(void)
{
    ADC10CTL0 |= ADC10SC;
    TA0CTL &= ~TAIFG;
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    if (CALBC1_1MHZ==0xFF)  //
            {                   //
                while(1);       //  DCO instellen. Wordt gebruikt voor SMCLK
            }                   //
        BCSCTL1 = CALBC1_1MHZ;  //
        DCOCTL = CALDCO_1MHZ;   //

    ADC10CTL1 = INCH_5 | ADC10SSEL_0 | ADC10DIV_7 | SHS_0;
    ADC10CTL0 = SREF_1 | REFON | ENC | ADC10ON | ADC10SR | REFBURST | ADC10IE;

    TA0CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;
    TA0CCR0 = 62500;

    initDisplay();
    setTitle("Temperatuur:");

    __enable_interrupt();

    while(1)
    {
        __low_power_mode_0();
    }

}
